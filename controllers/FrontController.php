<?php

/**
 * Created by PhpStorm.
 * User: gabrieleciaravolo
 * Date: 15/02/17
 * Time: 10:39
 */
class FrontController
{

    static function dispatch(){
        global $logged;
        global $db;
        $action = Tools::getValue("action");
        if($logged || $action == "login"){
            switch ($action) {
                case "login":
                    LoginController::login();
                    break;
                case "logout":
                    LoginController::logout();
                    break;
                case "saveTimeSheet":
                    $progetto = Tools::getValue("progetto");
                    $ore = Tools::getValue("ore");
                    $data = Tools::getValue("data");
                    if($progetto!=""){
                        if($ore!=""){
                            if(is_numeric($ore)){
                                if($data!=""){
                                    $date_regex = '/(0[1-9]|1[012])[- \/.](0[1-9]|[12][0-9]|3[01])[- \/.](19|20)\d\d/';
                                    if(preg_match($date_regex, $data)) {
                                        $sql = "INSERT INTO users_project (`id_user`,`id_project`,`time`,`date`) VALUES (".$_SESSION["id"].",".$progetto.",".$ore.",'".date("d/m/y",strtotime($data))."')";
                                        $db->query($sql);
                                        die(json_encode(array("success"=>true,"data"=>date("d/m/y",strtotime($data)) )));

                                    } else {
                                        die(json_encode(array("success"=>false,"msg"=>"La data non è in formato corretto (gg/mm/yyyy)." )));
                                    }
                                }else{
                                    die(json_encode(array("success"=>false,"msg"=>"Inserire una data." )));
                                }
                            }else{
                                die(json_encode(array("success"=>false,"msg"=>"Le ore inserite devono essere in formato numerico." )));
                            }
                        }else{
                            die(json_encode(array("success"=>false,"msg"=>"Inserire le ore lavorate." )));
                        }
                    }else{
                        die(json_encode(array("success"=>false,"msg"=>"Selezionare un progetto." )));
                    }
                    break;
                case "saveProject":
                    $progetto = Tools::getValue("progetto");
                    if($progetto!="") {
                        $sql = 'INSERT INTO project (`name`) VALUES ("' . $progetto. '")';
                        $db->query($sql);
                        $sql = "SELECT MAX(id) as id FROM project";
                        $res = $db->query($sql);
                        die(json_encode(array("success" => true,"new_id"=>$res[0]["id"])));
                    }else{
                        die(json_encode(array("success"=>false,"msg"=>"Inserire un nome per il Progetto." )));
                    }
                    break;
                case "saveImg":
                    $url = Tools::getValue("url");
                    if($url!="") {
                        $sql = 'UPDATE users SET `img` = "' . $url. '" WHERE id = '.$_SESSION["id"];
                        $db->query($sql);
                        die(json_encode(array("success" => true)));
                    }else{
                        die(json_encode(array("success"=>false,"msg"=>"Errore nel salvataggio dell'immagine." )));
                    }
                    break;
                case "saveUser":
                    $username = Tools::getValue("username");
                    $nome = Tools::getValue("nome");
                    $privilegi = Tools::getValue("privilegi");
                    $password = Tools::getValue("password");
                    if($username!="") {
                        if($nome!="") {
                            if($password!="") {
                                if($privilegi!="") {
                                    $sql = 'INSERT INTO users (`user`,`pwd`,`type`,`nome`) VALUES ("' . $username. '","' . md5($password). '",' . $privilegi. ',"' . $nome. '")';
                                    $db->query($sql);
                                    $sql = "SELECT MAX(id) as id FROM users";
                                    $res = $db->query($sql);
                                    die(json_encode(array("success" => true,"new_id"=>$res[0]["id"])));
                                }else{
                                    die(json_encode(array("success"=>false,"msg"=>"Selezionare i privilegi per l'utente." )));
                                }
                            }else{
                                die(json_encode(array("success"=>false,"msg"=>"Inserire una Password per l'utente." )));
                            }
                        }else{
                            die(json_encode(array("success"=>false,"msg"=>"Inserire un nome Utente." )));
                        }
                    }else{
                        die(json_encode(array("success"=>false,"msg"=>"Inserire uno Username per l'Utente." )));
                    }
                    break;
                case "getTimeSheet":
                    $id = Tools::getValue("id");
                    if($id){
                        $sql = "SELECT p.name as nome_progetto,up.date as `data`, up.time as ore FROM users_project AS up
                                JOIN project as p ON up.id_project = p.id
                                WHERE id_user =".$id;
                        $res = $db->query($sql);
                        die(json_encode($res));
                    }
                    die(json_encode(array()));
                    break;
                case "deleteTimeSheet":
                    $id = Tools::getValue("id");
                    if($id){
                        $sql = "DELETE FROM users_project WHERE id=".$id;
                        $res = $db->query($sql);
                        if(is_string($res))
                            die(json_encode(array("success"=>false,"msg"=>"Errore nella cancellazione.")));
                        die(json_encode(array("success"=>true,"msg"=>"Entry eliminata.")));
                    }
                    die(json_encode(array()));
                    break;
                case "deleteProject":
                    $id = Tools::getValue("id");
                    if($id){
                        $sql = "DELETE FROM project WHERE id=".$id;
                        $res = $db->query($sql);
                        if(!is_string($res)){
                            $sql = "DELETE FROM users_project WHERE id_project=".$id;
                            $res = $db->query($sql);
                            if(!is_string($res))
                                die(json_encode(array("success"=>true,"msg"=>"Progetto eliminato.")));
                            else
                                die(json_encode(array("success"=>false,"msg"=>"Errore nella cancellazione.")));
                        }else
                            die(json_encode(array("success"=>false,"msg"=>"Errore nella cancellazione.")));
                    }
                    die(json_encode(array()));
                    break;
                case "deleteUser":
                    $id = Tools::getValue("id");
                    if($id){
                        $sql = "DELETE FROM users WHERE id=".$id;
                        $res = $db->query($sql);
                        if(!is_string($res)){
                            $sql = "DELETE FROM users_project WHERE id_user=".$id;
                            $res = $db->query($sql);
                            if(!is_string($res))
                                die(json_encode(array("success"=>true,"msg"=>"Utente eliminato.")));
                            else
                                die(json_encode(array("success"=>false,"msg"=>"Errore nella cancellazione.")));
                        }else
                            die(json_encode(array("success"=>false,"msg"=>"Errore nella cancellazione.")));
                    }
                    die(json_encode(array()));
                    break;
                case "getProjects":
                    $sql = "SELECT * FROM project " ;
                    $res = $db->query($sql);
                    if($res) {
                        die(json_encode($res));
                    }
                    die(json_encode(array()));
                    break;
                case "getUsers":
                    $sql = "SELECT * FROM users WHERE id <> ".$_SESSION["id"] ;
                    $res = $db->query($sql);
                    if($res) {
                        die(json_encode($res));
                    }
                    die(json_encode(array()));
                    break;
                default:
                    echo self::display();
                    break;
            }
        }else{
            echo self::display("login");
        }
    }
    private function getView($page){
        $file = _VIEW_DIR_."/tpl/404.php";
        if(file_exists(_VIEW_DIR_."/tpl/".$page.".php")) {
            $file = (_VIEW_DIR_."/tpl/" . $page . ".php");
        }
        ob_start();
        require($file);
        return ob_get_clean();
    }

    private function display($page = ""){
        if($page=="") {
            $page = Tools::getValue("p")."_".($_SESSION["type"] ? $_SESSION["type"] : 0);
        }
        self::preDisplay($page);
        $page_html = self::getView($page);
        $head = self::getView("header");
        $footer = self::getView("footer");

        return $head.$page_html.$footer;
    }

    private function preDisplay($page){
        global $db;
        switch($page){
            case "home_0":
                global $entries;
                $sql = "SELECT up.id as id_entry,p.name as nome_progetto,up.date as `data`, up.time as ore FROM users_project AS up
                                JOIN project as p ON up.id_project = p.id
                                WHERE id_user =".$_SESSION["id"];
                $entries = $db->query($sql);
                global $projects;
                $sql = "SELECT * FROM project";
                $projects = $db->query($sql);
                global $avatar;
                global $name;
                $sql = "SELECT img,nome FROM users WHERE id = ".$_SESSION["id"];
                $res = $db->query($sql);
                $avatar = $res[0]["img"];
                $name = $res[0]["nome"];
                break;
            case "home_1":
                $sql = "SELECT * FROM users WHERE id <> ".$_SESSION["id"];
                global $users;
                $users = $db->query($sql);
                break;
        }
    }
}