<?php
/**
 * Created by PhpStorm.
 * User: gabrieleciaravolo
 * Date: 15/02/17
 * Time: 10:33
 */
require_once "classes/Tools.php";
require_once "classes/DB.php";
require_once "controllers/FrontController.php";
require_once "controllers/LoginController.php";

define("_VIEW_DIR_",$_SERVER['DOCUMENT_ROOT']."/views");

session_start();

// DATABASE
global $db;
$db= new DB();
$db->connect();

//LOGGED
global $logged;
$logged = LoginController::isLogged();


