<?php

/**
 * Created by PhpStorm.
 * User: gabrieleciaravolo
 * Date: 15/02/17
 * Time: 09:59
 */
class DB
{
    private $conn;

    public function connect($server = "localhost",$user = "root",$pwd = "root",$dbname = "timetable"){
        $this->conn = mysqli_connect($server,$user,$pwd,$dbname);
    }
    public function query($sql){
        $res = mysqli_query($this->conn,$sql);
        $ret = array();
        if(mysqli_error($this->conn))
            return mysqli_error($this->conn);
        while($row = mysqli_fetch_array($res)){
            $ret[]=$row;
        }
        return $ret;
    }
}