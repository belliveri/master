<?php
global $users;
?>

<div class="container">
    <div class="row">
        <div class="col-sm-3">
            <div class="profile-sidebar">

                <div id="sidebar" class="well sidebar-nav">
                    <h5><i class="glyphicon glyphicon-home"></i>
                        <small><b>GESTIONE</b></small>
                    </h5>
                    <ul class="nav nav-pills nav-stacked">
                        <li ><a id="all-projects" onclick="getAllProjects(this)">Progetti</a></li>
                        <li ><a id="all-users"  onclick="getAllUsers(this)">Utenti</a></li>
                    </ul>
                    <h5><i class="glyphicon glyphicon-user"></i>
                        <small><b>UTENTI</b></small>
                    </h5>
                    <ul class="nav nav-pills nav-stacked">
                        <?php
                            foreach ($users as $u){
                        ?>
                        <li>
                            <a class="users-a" data-id="<?=$u['id']?>">
                                <?= $u['nome']?>
                            </a>
                        </li>
                        <?php
                        }
                        ?>
                    </ul>
                </div>
            </div>
        </div>
        <div class="col-sm-9">

            <div id="time-sheet-res">
                <div class="text-center" style="font-size: 35px;">
                    <i class="glyphicon glyphicon-dashboard" style="margin-right: 10px;"></i>
                <br>
                Time Tracker
                </div>
            </div>
        </div>
    </div>
</div>