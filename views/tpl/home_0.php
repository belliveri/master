<?php
global $entries;
global $avatar;
global $projects;
global $name;
?>
<script>
    projects = <?=json_encode($projects)?>
</script>
<div class="container">
    <div class="row profile">
        <div class="col-sm-2">
            <div class="profile-sidebar">
                <div class="profile-userpic text-center">
                    <h2><?=$name?></h2>
                    <div id="user_avatar" style="background-image: url('<?=($avatar && $avatar!="" ? $avatar : "/views/img/user_image_placeholder.png")?>')"></div>
                    <!-- The fileinput-button span is used to style the file input field as button -->
                    <span class="btn btn-success fileinput-button">
                        <i class="glyphicon glyphicon-plus"></i>
                        <span>Carica Img</span>
                                        <!-- The file input field used as target for the file upload widget -->
                        <input id="fileupload" type="file" name="files[]" multiple>
                    </span>
                    <!-- The global progress bar -->
                    <div id="progress" class="progress">
                        <div class="progress-bar progress-bar-success"></div>
                    </div>
                    <!-- The container for the uploaded files -->
                    <div id="files" class="files"></div>
                    <br>
                </div>
            </div>
        </div>
        <div class="col-sm-10">
            <div id="time-sheet-res">
                <h3>Elenco progetti</h3>
                <table class="table">
                    <thead>
                    <tr>
                        <th>#</th>
                        <th>Progetto</th>
                        <th>Ore lavorate</th>
                        <th>Data</th>
                        <th>Azione</th>
                    </tr>
                    </thead>
                    <tbody id="time-sheet-res-body">
                    <?php
                    $i = 1;
                    foreach ($entries as $e){

                        ?>
                    <tr>
                        <th scope="row"><?echo $i++?></th>
                        <td class='col-xs-4'><?=$e["nome_progetto"]?></td>
                        <td class='col-xs-2'><?=$e["ore"]?></td>
                        <td class='col-xs-4'><?=date("d/M/Y",strtotime($e["data"]))?></td>
                        <td class='col-xs-2'>
                            <button class="delete_entry btn btn-danger" data-rel="time_entry" data-id="<?=$e["id_entry"]?>">
                            <span class="" ><i class="glyphicon glyphicon-trash" style="margin: 0px 10px 0px 0px;"></i>Elimina</span>
                            </button>
                        </td>
                    </tr>
                        <?php
                    }
                    ?>
                    <tr id="new_entry">

                    </tr>
                    </tbody>
                </table>
            </div>
            <div class='col-xs-12 pull-right'>
                <button id="new-entry-btn" class="btn btn-success">
                    <span><i class="glyphicon glyphicon-plus" style="margin: 0px 10px 0px 0px;"></i>Aggiungi</span>
                </button>
            </div>
        </div>
    </div>
</div>