var projects;
$(function(){
    $("#loginForm").submit(function(){
        var form = $(this);
        $.ajax({
            type: "POST",
            url:  "/?action=login&ajax=true",
            data: form.serialize(),
            dataType: 'json', //restituisce un oggetto JSON
            success: function (data) {
                if(data.success){
                    //alert(data.msg);
                    showAlert(data.msg,"alert-info");
                    setTimeout(function(){
                        window.location = "/home";
                    },1000);
                }else{
                    showAlert(data.msg,"alert-danger");
                }
            },
            error: function(data){

            }
        });
        return false;
    });
    $(".users-a").click(function(){
        $(this).append('<span id="loading-spinner" class="glyphicon glyphicon-refresh glyphicon-refresh-animate" hidden="true"></span>');
        $("#loading-spinner").fadeIn();
        var id = $(this).data("id");
        var nome = $(this).html();
        $("#time-sheet-res").fadeOut(function(){
            $.ajax({
                type: "POST",
                url:  "/?action=getTimeSheet&ajax=true",
                data: "id="+id,
                dataType: 'json', //restituisce un oggetto JSON
                success: function (data) {

                    var title_html  = "<h3>Report lavorativo - "+nome+"</h3>";
                    var table_html = "<table class='table'>";
                    var thead_html = "<thead><tr><th>#</th><th>Progetto</th><th>Ore</th><th>data</th></tr></thead>";
                    var tbody_html = "<tbody id='time-sheet-res-body'>";
                    var str_html = title_html + table_html + thead_html + tbody_html;
                    data.forEach(function (el,i) {
                        var index = i + 1;
                        str_html += "<tr><th scope='row'>"+index+"</th>" +
                        "<td class='col-xs-4'>"+el.nome_progetto+"</td>" +
                        "<td class='col-xs-4'>"+el.ore+"</td>" +
                        "<td class='col-xs-4'>"+el.data+"</td></tr>" ;
                    });
                    str_html += "</tbody></table>" ;

                    $("#time-sheet-res").html(str_html);
                    $("#loading-spinner").fadeOut(function(){
                        $(this).remove();
                    });
                    $("#time-sheet-res").fadeIn();
                },
                error: function(data){

                }
            });
        });
    });
    $("#new-entry-btn").click(function(){
        $(this).hide();
        $("#new_entry").remove();
        var str_html = "<tr id='new_entry'><th scope='row'>#</th>"+
            "<td scope='row' class='col-xs-4'>" +
            "<select name='progetti' id='progetto_entry'>";
        projects.forEach(function (el,i) {
            str_html+="<option value='"+el.id+"'>"+el.name+"</option>";
        });
        str_html+="</select>" +
            "<td class='col-xs-2'><input type='text' id='ore_entry' placeholder='Ore Lavorate'/></td>" +
            "<td class='col-xs-4'><input type='text' id='data_entry' placeholder='mm/dd/yyyy'/></td>" +
            "<td class='col-xs-2'>" +
            "<div class='pull-left'>" +
            "<button id='save_entry' class='btn btn-success'>" +
            "<span><i class='glyphicon glyphicon-ok'></i></span>"+
            "</button>"+
            "</div>" +
            "<div class='pull-right'>" +
            "<button class='btn btn-warning' id='cancel_new_entry'>" +
            "<span><i class='glyphicon glyphicon-remove'></i></span>"+
            "</button>" +
            "<div>" +
            "</td>";
        $("#time-sheet-res-body").append(str_html);
    });
    $(document).on("click","#new-project-btn",function(){
        $(this).hide();
        $("#new_project").remove();
        var str_html = "<tr id='new_project'><td >#</td>"+
            "<td class=''><input type='text' id='nome_progetto' placeholder='Nome Progetto'/></td>" +
            "<td><div class='pull-right'>" +
            "<button class='btn btn-warning' id='cancel_new_project'>" +
            "<span><i class='glyphicon glyphicon-remove'></i></span>"+
            "</button>" +
            "</div>" +
            "<div class='pull-right'>" +
            "<button id='save_project' class='btn btn-success'>" +
            "<span><i class='glyphicon glyphicon-ok'></i></span>"+
            "</button>"+
            "</div>" +
            "</td>" +
            "</tr>";
        $("#time-sheet-res-body").append(str_html);
    });

    $(document).on("click","#new-user-btn",function(){
        $(this).hide();
        $("#new_user").remove();
        var str_html = "<tr id='new_user'><td >#</td>"+
            "<td class=''><input type='text' id='username' placeholder='Username'/>" +
            "<input type='text' id='password_utente' placeholder='Password Utente'/>" +
            "<input type='text' id='nome_utente' placeholder='Nome Utente'/>" +
            "</td>" +
            "<td><select name='privilegi' id='privilegi'>" +
            "<option value='0'>base</option>" +
            "<option value='1'>Amministratore</option>" +
            "</select></td>" +
            "<td><div class='pull-right'>" +
            "<button class='btn btn-warning' id='cancel_new_user'>" +
            "<span><i class='glyphicon glyphicon-remove'></i></span>"+
            "</button>" +
            "</div>" +
            "<div class='pull-right'>" +
            "<button id='save_user' class='btn btn-success'>" +
            "<span><i class='glyphicon glyphicon-ok'></i></span>"+
            "</button>"+
            "</div>" +
            "</td>" +
            "</tr>";
        $("#time-sheet-res-body").append(str_html);
    });
    $(document).on("click",".delete_entry",function(){
        var id = $(this).data("id");
        var el = $(this);
        var type = $(this).data("rel");
        var action ="";
        var successCall = function(data){
            if(data.success){
                showAlert(data.msg,"alert-success");
                el.parents("tr")[0].remove();
            }else{
                showAlert(data.msg,"alert-danger");
            }
        };
        switch(type){
            case "projects":
                action ="deleteProject";
                break;
            case "time_entry":
                action ="deleteTimeSheet";
                break;
            case "users":
                action ="deleteUser";
                break;
        }

        $.ajax({
            type: "POST",
            url:  "/?action="+action+"&ajax=true",
            data: "id="+id,
            dataType: 'json', //restituisce un oggetto JSON
            success: function (data) {
                successCall(data);
            },
            error: function(data){

            }
        });

    });
    $(document).on("click","#save_entry",function(){
        $.ajax({
            type: "POST",
            url:  "/?action=saveTimeSheet&ajax=true",
            data: "progetto="+$("#progetto_entry").val()+"&ore="+$("#ore_entry").val()+"&data="+$("#data_entry").val(),
            dataType: 'json', //restituisce un oggetto JSON
            success: function (data) {
                if(data.success){
                    window.location.reload();
                }else{
                    showAlert(data.msg,"alert-danger");
                }
            },
            error: function(data){

            }
        });
    });
    $(document).on("click","#save_project",function(){
        $.ajax({
            type: "POST",
            url:  "/?action=saveProject&ajax=true",
            data: "progetto="+$("#nome_progetto").val(),
            dataType: 'json', //restituisce un oggetto JSON
            success: function (data) {
                if(data.success){

                    $("#time-sheet-res-body").append("<tr>" +
                        "<td>"+data.new_id+"</td>" +
                        "<td>"+$("#nome_progetto").val()+"</td>" +
                        '<td class="text-right"><button class="delete_entry btn btn-danger" data-rel="projects" data-id="'+data.new_id+'">'+
                        '<span class=""><i class="glyphicon glyphicon-trash" style="margin: 0px 10px 0px 0px;"></i>Elimina</span>'+
                        '</button></td>'+
                        "</tr>");
                    $("#new_project").remove();
                    $("#new-project-btn").show();
                }else{
                    showAlert(data.msg,"alert-danger");
                }
            },
            error: function(data){

            }
        });
    });
    $(document).on("click","#save_user",function(){
        $.ajax({
            type: "POST",
            url:  "/?action=saveUser&ajax=true",
            data: "username="+$("#username").val()+"&nome="+$("#nome_utente").val()+"&password="+$("#password_utente").val()+"&privilegi="+$("#privilegi").val(),
            dataType: 'json', //restituisce un oggetto JSON
            success: function (data) {
                if(data.success){
                    $("#time-sheet-res-body").append("<tr>" +
                        "<td>"+data.new_id+"</td>" +
                        "<td>"+$("#nome_utente").val()+"</td>" +
                        "<td class='text-right'>"+$("#privilegi option:selected").html()+"</td>" +
                        '<td class="text-right"><button class="delete_entry btn btn-danger" data-rel="users" data-id="'+data.new_id+'">'+
                        '<span class=""><i class="glyphicon glyphicon-trash" style="margin: 0px 10px 0px 0px;"></i>Elimina</span>'+
                        '</button></td>'+
                        "</tr>");
                    $("#new_user").remove();
                    $("#new-user-btn").show();
                }else{
                    showAlert(data.msg,"alert-danger");
                }
            },
            error: function(data){

            }
        });
    });

    $(document).on("click","#cancel_new_entry",function(){
        $("#new_entry").remove();
        $("#new-entry-btn").show();
    });
    $(document).on("click","#cancel_new_project",function(){
        $("#new_project").remove();
        $("#new-project-btn").show();
    });

    $(document).on("click","#cancel_new_user",function(){
        $("#new_user").remove();
        $("#new-user-btn").show();
    });


})


function showAlert(message,alerttype) {

    $('#alert_placeholder').append('<div id="alertdiv" class="alert ' +  alerttype + '"><a class="close" data-dismiss="alert">×</a><span>'+message+'</span></div>')
    setTimeout(function() { // this will automatically close the alert and remove this if the users doesnt close it in 5 secs
        $("#alertdiv").remove();
    }, 5000);
}


function getAllProjects(el){
    $(el).append('<span id="loading-spinner" class="glyphicon glyphicon-refresh glyphicon-refresh-animate" hidden="true"></span>');
    $("#loading-spinner").fadeIn();
    $("#time-sheet-res").fadeOut(function() {
        $.ajax({
            type: "GET",
            url: "/?action=getProjects&ajax=true",
            dataType: 'json', //restituisce un oggetto JSON
            success: function (data) {
                var title_html = "<h3>Lista progetti</h3>";
                var table_html = "<table class='table'>";
                var thead_html = "<thead><tr><th>ID Progetto</th> <th>Nome progetto</th> <th class='text-right'>Azione</th></tr></thead>";
                var tbody_html = "<tbody id='time-sheet-res-body'>";
                var str_html = title_html + table_html + thead_html + tbody_html;
                data.forEach(function (el, i) {
                    var index = i + 1;
                    str_html += "<tr><td>" + el.id + "</td>" +
                        "<td >" + el.name + "</td>" +
                        '<td class="text-right"><button class="delete_entry btn btn-danger" data-rel="projects" data-id="'+el.id+'">'+
                        '<span class=""><i class="glyphicon glyphicon-trash" style="margin: 0px 10px 0px 0px;"></i>Elimina</span>'+
                        '</button></td></tr>';
                });
                str_html += "</tbody></table>";

                str_html+='<div class="col-xs-12 pll-right">'+
                    '<button id="new-project-btn" class="btn btn-success">'+
                    '<span><i class="glyphicon glyphicon-plus" style="margin: 0px 10px 0px 0px;"></i>Aggiungi</span>'+
                    '</button>'+
                    '</div>';

                $("#time-sheet-res").html(str_html);
                $("#loading-spinner").fadeOut(function(){
                    $(this).remove();
                });
                $("#time-sheet-res").fadeIn();
            },
            error: function (data) {

            }
        });
    });
}

function getAllUsers(el){
    $(el).append('<span id="loading-spinner" class="glyphicon glyphicon-refresh glyphicon-refresh-animate" hidden="true"></span>');
    $("#loading-spinner").fadeIn();
    $("#time-sheet-res").fadeOut(function(){

        $.ajax({
            type: "GET",
            url:  "/?action=getUsers&ajax=true",
            dataType: 'json', //restituisce un oggetto JSON
            success: function (data) {
                var title_html  = "<h3>Lista utenti</h3>";
                var table_html = "<table class='table'>";
                var thead_html = "<thead><tr><th>#</th> <th>Nome utente</th><th class='text-right'>Privilegi</th><th class='text-right'>Azione</th></tr></thead>";
                var tbody_html = "<tbody id='time-sheet-res-body'>";
                var str_html = title_html + table_html + thead_html + tbody_html;
                data.forEach(function (el,i) {
                    var index = i + 1;
                    if(el.type == 1) {
                        var credenziali = "Amministratore";
                    }
                    else{
                        var credenziali = "base";
                    }
                    str_html += "<tr><td>"+el.id+"</td>" +
                        "<td >"+el.nome+"</td>" +
                        "<td class='text-right'>"+credenziali+"</td>"+
                        '<td class="text-right"><button class="delete_entry btn btn-danger" data-rel="users" data-id="'+el.id+'">'+
                        '<span class=""><i class="glyphicon glyphicon-trash" style="margin: 0px 10px 0px 0px;"></i>Elimina</span>'+
                        '</button></td></tr>';
                });
                str_html += "</tbody></table>" ;

                str_html+='<div class="col-xs-12 pll-right">'+
                    '<button id="new-user-btn" class="btn btn-success">'+
                    '<span><i class="glyphicon glyphicon-plus" style="margin: 0px 10px 0px 0px;"></i>Aggiungi</span>'+
                    '</button>'+
                    '</div>';

                $("#time-sheet-res").html(str_html);
                $("#loading-spinner").fadeOut(function(){
                    $(this).remove();
                });
                $("#time-sheet-res").fadeIn();
            },
            error: function(data){

            }
        });
    });
}
$(function () {
    'use strict';
    // Change this to the location of your server-side upload handler:
    var url = window.location.hostname === 'blueimp.github.io' ?
        '//jquery-file-upload.appspot.com/' : '/views/fileupload/server/php/';
    $('#fileupload').fileupload({
        url: url,
        dataType: 'json',
        done: function (e, data) {
            var url = (data.result.files[0].url);
            $.ajax({
                type: "GET",
                url: "/?action=saveImg&ajax=true",
                data: "url="+url,
                dataType: 'json', //restituisce un oggetto JSON
                success: function (data) {
                    $("#user_avatar").css("background-image","url("+url+")");
                    $("#progress").fadeOut(function(){
                        $('#progress .progress-bar').css(
                            'width',
                            0 + '%'
                        );
                    });
                }
            });
        },
        progressall: function (e, data) {
            var progress = parseInt(data.loaded / data.total * 100, 10);
            $("#progress").fadeIn();
            $('#progress .progress-bar').css(
                'width',
                progress + '%'
            );
        }
    }).prop('disabled', !$.support.fileInput)
        .parent().addClass($.support.fileInput ? undefined : 'disabled');
});
function displayError(msg){
    $("#myModalLabel").css("color","red");
    $("#myModalLabel").html("Attenzione");
    $("#myModal .modal-body").html(msg);
    $("#myModal").modal();
}